# 專題名稱
duckside投資管理平台

# 專題起源
2021年10月參加資策會旗下資展國際的前端工程師訓練班(尖兵計畫)

# 專案簡介
這是一個功能取向的投資管理網站，以自主投資的業餘人士為主要客群，協助其擬定交易計畫、紀錄交易、追蹤投資組合、檢討績效，也可以透過模擬投資遊戲來回測自己的想法。

# 專題指導
許雅婷、錢達智、趙令文、陳思芳、何柏杰

# 專題夥伴
組長：鄭人豪　組員：李巧琳、蔡沛珊、陳鎧洋、李晴暄、李冠樺

# 安裝套件
`npm install`

# 啟動專案
`npm start`


# 使用套件
## react-router-dom@6

React 路由管理，注意是第6版，不是第5版

參考教學： [https://reactrouter.com/docs/en/v6/getting-started/tutorial](https://reactrouter.com/docs/en/v6/getting-started/tutorial)

## concurrentlyc    

利用 concurrently 套件，一次執行2個 server
1. 後端 backend 的 server，用來處理資料庫相關 routes。預設 port 為 5000
2. 前端 react 的 server，預設 port 為 3000


# 檔案結構

## 後端

### `/backend/app.js`

處理連接資料庫相關程式

### `/backend/router...`

處理各資料表的增刪查改

## 前端

### `/public/`

靜態檔案

### `/src/App.jsx`

管理主要頁面路由

### `/src/components/`

放置 React 元件

### `/src/pages/`

放置 React 頁面，利用元件組成頁面

### `/src/routes/`

管理各分類下的頁面路由

# Notion
筆記位置：[https://thin-nerve-d81.notion.site/Duckside-d0b9a77949b44fcbaae54dcc4b414d0d](https://thin-nerve-d81.notion.site/Duckside-d0b9a77949b44fcbaae54dcc4b414d0d)

# Heroku
網站位置：[https://duckside0316.herokuapp.com/](https://duckside0316.herokuapp.com/)